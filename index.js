// console.log("Hello world")

// [SECTION] Document Object Model
// It allows us to be able to access or modify a properties of an element in a webpage.
// it is also a standard on how to get, change, add, or delete HTML elements.

/*
	Syntax:
		document.querySelector("htmlElement");

		- The querySelector function takes a string input that is formatted like a css Selector when applying styles.
*/


// Alternative way on retrieving HTML elements

// document.getElementById("txt-first-name");
// document.getElementClassName("txt-last-name");


// However,  using these functions requires us to identify beforehand we get the element. With querySelector, we can be flexible in how retrieve elements.

const txtFirsName = document.querySelector("#txt-first-name");
const txtLastName = document.querySelector("#txt-last-name");
const spanFullName = document.querySelector("#span-full-name");

// [SECTION] Event listeners
// Whenever a user interacts with a webpage, this action is considered as an event. (example: mouse click, mouse hover, page load, key press, etc.)

// addEventListener takes two arguments:
	// a string that identify an event.
	// function that the listener will execute once the "specified event" is triggered.
// txtFirsName.addEventListener("keyup", () =>{
// 	// "innerHTML" property sets or returns the HTML content(inner HTML) of an element(div, spans, etc.)
// 	// ".value" property sets or returns the value of an attribute (form controls).
// 	spanFullName.innerHTML = `${txtFirsName.value}`;
// })

// // When the event occurs, an "event object" is passed to the function argument as the first parameter.
// txtFirsName.addEventListener("keyup", (event) =>{
// 	// the "event.target" contains the element where the event happened.
// 	console.log(event.target);
// 	// The "event.target.value" gets the value of the input object(txt-first-name)
// 	console.log(event.target.value);
// })

// Creating Multiple event that use the same function.

const fullName = () => {
	spanFullName.innerHTML = `${txtFirsName.value} ${txtLastName.value}`

}

txtFirsName.addEventListener("keyup", fullName);
txtLastName.addEventListener("keyup", fullName);


/*
Create another addEventListener that will "change the color" of the "spanFullName". Add a "select tag" element with the options/values of red, green, and blue in your index.html.

Check the following links to solve this activity:
    HTML DOM Events: https://www.w3schools.com/jsref/dom_obj_event.asp
    HTML DOM Style Object: https://www.w3schools.com/jsref/dom_obj_style.asp

*/

const red = document.querySelector("color-red");
const green = document.querySelector("color-green");
const blue = document.querySelector("color-blue");

const color = document.querySelector("colors")

const textColor = () => {
	color.innerHTML = `${red}, ${green}, ${blue}`
}